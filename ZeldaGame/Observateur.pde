interface Observateur {
  void update (Observable observable);
}
