//observer
class Personnage extends GraphicObject implements Observable{

ArrayList<P_Model> model;
int w_sprite=17;
int h_sprite=24;
int sizeX=17;
int sizeY=24;
int sizec=4;
int sizer=7;
int resize=2;
PImage mov_sheet[][];
int stateNum;
State state ;
 ArrayList<Observateur> observateurs = new ArrayList<Observateur>();


 Personnage(ArrayList<P_Model> md,PVector loc){
  
    model = md;
    mov_sheet= new PImage[sizer][sizec];
    load_img(model);
    location=loc;
    state = new State();
  }
 
 
 void load_img(ArrayList<P_Model> md){
 
   
   for(int i=0;i<model.size();i++){
     for(int j=0;j<sizer;j++)
     {
       mov_sheet[j][i]=model.get(i).texture.get(j*w_sprite,0,sizeX,sizeY);
 
     }
 }
   

 }
 
 int getHeight(){
 
   return sizeY*resize;
 
 }
 
  int getWidth(){
 
   return sizeX*resize;
 
 }
 
 PVector getLocation(){
 
 return location;
 }
  void add_locationX(int mv)
 {
  this.location.x+=mv;   

 }
 
 void add_locationY(int mv)
 {
  this.location.y+=mv;   

 }
 
 void update(float deltaTime){
    if (counter >= 6)
      counter=0;
      // constrain(valeur location, minimun, maximun)
    location.x = constrain(location.x,0,width-sizeX*resize);  
    location.y = constrain(location.y,0,height-sizeY*resize);  
 }
 
 void moveUp(){
    img_display=1;
    if(time_beetween + interval_display <= millis())
      {
        time_beetween=millis();
        counter++;
        link.add_locationY(-4);
      }
 }
 void moveDown(){
     img_display=0;
     if(time_beetween + interval_display <= millis())
     {
       time_beetween=millis();
       counter++;
       link.add_locationY(4);
     }
 }
 
 void moveLeft(){
   img_display=2;
   if(time_beetween + interval_display <= millis())
     {
       time_beetween=millis();
       counter++;
       link.add_locationX(-4);
     }
 
 }
 
 void moveRight(){
   img_display=3;
   if(time_beetween + interval_display <= millis())
    {
      time_beetween=millis();
      counter++;
      link.add_locationX(4);
    }
 }
 
 void setPersState(int _state){
    stateNum = _state;
  }
 
 
 void display(){}
  
 void display(int i , int j){
 mov_sheet[j][i].resize(sizeX*resize,sizeY*resize);
 image(mov_sheet[j][i],location.x,location.y);
 
 
 }
 
  void addObs (Observateur obs){
      observateurs.add(obs);
  }
  
  void deleteObs (Observateur obs){
     if (observateurs.contains(obs)) {
      observateurs.remove(obs);
    }
  
  }
  
  void call(){
   
     for (Observateur o : observateurs) {
      o.update(this);
    }
  }
  
 State getState(){
   return state;
 }


}
