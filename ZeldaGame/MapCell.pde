class MapCell {
  int tileID;

  public MapCell(int tileID){
    this.tileID = tileID;
  }
  
  public  int getTileID(){
    return tileID;
  }
  
 public void setTileID(int tileID){
   this.tileID = tileID;
 }


}
