public static class  Tile {

 public static PImage tileSet;
 public static int tileWidth =32;
 public static int tileHeight = 32;
 public static int tileX=0,tileY=0;
 public static Rectangle tileRect;
  
  
  public static void setRectangleTile(Rectangle rect){
    tileRect =  rect;
  }
  
  
  public static int getTileWidth(){
    return tileWidth;
  }
  
  public static int getTileHeight(){
    return tileHeight;
  }
  
  public static void setTileWidth(int _tileWidth){
    tileWidth = _tileWidth ;
  }
  
  public static void setTileHeight(int _tileHeight){
    tileHeight = _tileHeight ;
  }
  
  public static Rectangle getSourceRect(int index){
    tileX = index % (tileSet.width / tileWidth);
    tileY = index / (tileSet.width / tileWidth);
    
    tileRect.setrectWH(tileWidth, tileHeight);
    tileRect.setrectXY(tileX * tileWidth, tileY * tileHeight);
  
  return tileRect;
  }
  
  public static  PImage getTileTexture() {
    return tileSet;
  }
  
  public static void setTileTexture(PImage _tileSet)
  {
    tileSet = _tileSet;
  }
  
 






}
