
int currentTime;
int deltaTime;
int rectWidth, rectHeight, tileSize;
ArrayList<PImage> textureList;
Personnage link;
int x, y , counter, img_display,interval_display;
float time_beetween;
 PVector personnageTiled;
ArrayList <P_Model> link_mov;
int firstX;
int firstY;
RemoteControl remotecontrole;
boolean flagTile ;
PImage blueTile;
Command leftMove;
Command rightMove;
Command upMove;
Command downMove;
Command spaceTrigger;

TileMap map;
TextureMap textMap;
Tile tile;
Rectangle rect;


int[] mazeSizes = { 6,8,10,12,15,20};
int mazeIndex=0;
int sizeXsreen;
int sizeYscreen;
Maze maze;
MazeBall ball;
 
boolean finished;

long timer;
 
 HashMap<Character , Command> commandMap;
 

void setup()
{
  size(960, 720, P2D);
  currentTime = millis();
  deltaTime = millis();
  timer = millis();
  linksetup();
  remotecontrole = new RemoteControl();
  sizeYscreen=height;
  sizeXsreen=width;
  maze = new Maze(width/2-300,height/2-300,600,mazeSizes[0]);
 
  ball = new MazeBall(maze.x+maze.step/2, maze.y+maze.step/2,3*maze.step/4,color(255,127,0));
 link.addObs(maze);
  finished = false;
  timer = millis();
  flagTile= true;
  tileSize = 32;
  rectWidth = 75;
  rectHeight = 75;
  rect = new Rectangle();
  textMap= new TextureMap("data/tile_map.png");
  textureList = new ArrayList<PImage>();
  
  Tile.setTileTexture(textMap.tileSet);
  Tile.setRectangleTile(rect);
  blueTile=Tile.getTileTexture().get(96,0,32,32);
  PVector firstSquare = new PVector(Camera.getLocation().x / (float)tileSize,
    Camera.getLocation().y / (float)tileSize);
    
  personnageTiled = new PVector(link.getLocation().x/ (float)tileSize,link.getLocation().y/ (float)tileSize);
    
   firstX = (int) firstSquare.x;
   firstY = (int) firstSquare.y;
  
  map = new TileMap();
   Rectangle thisRectangle;
   for(int i = 0; i < rectHeight; i++)
    {
      for(int j = 0; j < rectWidth; j++)
      {
        thisRectangle = Tile.getSourceRect(map.getRow(j + firstY).getMapCell(i + firstX).getTileID());
        PImage currentTile = Tile.getTileTexture().get(
        (int)thisRectangle.location.x, 
        (int)thisRectangle.location.y,
        (int)thisRectangle.rectWidth, 
        (int)thisRectangle.rectHeight);
        
        textureList.add(currentTile);
      }
    }

}


void draw(){
  background(255);

  display();
  
  if(keyPressed && (maze.stateNum == 0 || maze.complete))
    {
      remotecontrole.InputHandler();
    }
    
  update(0);

  personnageTiled = new PVector(link.getLocation().x/ (float)tileSize,(link.getLocation().y + (link.getHeight()*0.80) )/ (float)tileSize); 
  if((map.getRow((int)personnageTiled.x).getMapCell((int)personnageTiled.y).getTileID()!=0) && flagTile  ) 
  {
    print("contact tiles");
    link.call();
  //  maze.state.nextState(maze);
    flagTile = false;
    
  }
}

void linksetup(){

   img_display=0;
   counter=0;
   x=0;
   y=0;
   interval_display=50;
   
   link_mov = new ArrayList<P_Model>();
   fill_link_mov();
   
  link = new Personnage( link_mov, new PVector(width/2,height/2));
  time_beetween=0;
  x=(counter % 7)*link.w_sprite;
  time_beetween=millis();

}


void display()
{
 // print(maze.stateNum);
  if(maze.stateNum == 1){
    mazeDisplay();
  }
  else{
   cameraDisplay();
   for (int i = 0; i < rectHeight; i++)
    {
      int positionY = (i * tileSize);
      for (int j = 0; j < rectWidth; j++)
      {
        if(i==map.randJ && j==map.randI)
        {
          image(blueTile,(j * tileSize) - Camera.location.x, positionY - Camera.location.y);
        }
        else
        image(textureList.get(i + j), (j * tileSize) - Camera.location.x, positionY - Camera.location.y);
      }
    }
    
  link.display(img_display,counter);  
  }
}


void cameraDisplay()
  {
    PVector cameraLocation = link.location.get();
    cameraLocation.x -= width/2;
    cameraLocation.y -= height/2;
    Camera.setLocation(cameraLocation);
    
    PVector minLimit = new PVector(200, 200);
    PVector maxLimit = new PVector(width + 200, height + 200);
    Camera.location.x = Math.max(minLimit.x - 200, Camera.location.x);
    Camera.location.x = Math.min(maxLimit.x - width, Camera.location.x);
    Camera.location.y = Math.max(minLimit.y - 200, Camera.location.y);
    Camera.location.y = Math.min(maxLimit.y - height, Camera.location.y);
  }
  
  
void update(int delta){
   link.update(0);
 
}

void keyPressed(){

  if (!finished && maze.stateNum == 1) {
    boolean[] walls = maze.travelThrough(ball.x, ball.y);
     
     remotecontrole.InputHandler(walls);
 }
}

void mazeDisplay(){

  if (!maze.complete) {
    maze.routeStep();
  }
 
  else {
    if (!finished) {
      background(255);
      maze.display();
 
      //indicate destination cell
      fill(200,255,200);
      stroke(0,255,0);
      strokeWeight(3);
      ellipse(maze.destinationX+maze.step/2,maze.destinationY+maze.step/2,3*maze.step/4+2,3*maze.step/4+2);
 
      //show player position
      ball.display();
 
      if (ball.x>maze.destinationX && ball.y>maze.destinationY) {
        finished = true;
      }
    }
    else {
      noStroke();
      fill(127,255,127,10);
      rect(0,0,width,height);
      fill(75);
      rect(width/6,height/2-125,4*width/6,250);
      textAlign(CENTER,CENTER);
      fill(0);
      text("you win to return to the main game press q ",width/2,height/2);
    }
  }
}


void fill_link_mov(){
 //créer mes texture selon mes images png pour link
 P_Model md = new P_Model("data/front_link.png");
 link_mov.add(md);
 
   md = new P_Model("data/back_link.png");
 link_mov.add(md);
 
   md = new P_Model("data/left_link.png");
 link_mov.add(md);
 
   md = new P_Model("data/right_link.png");
 link_mov.add(md);
}
