public class State
{
  private IState etat = new SleepState();
  
  
  public void nextState(Maze m)
  {
    etat.next(this,m);

  }
  
  IState getState()
  {
    return etat;
  }
  
  void setState(IState etat)
  {
    this.etat = etat;
  }
}
