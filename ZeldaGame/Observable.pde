interface Observable {
  void addObs (Observateur obs);
  void deleteObs (Observateur obs);
  void call();
}
