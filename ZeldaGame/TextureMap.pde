class TextureMap{

  PImage tileSet ;
  int textureWidth, textureHeight;
  
  public TextureMap(String image)
  {
    
    tileSet = loadImage(image);
    
    textureWidth = 32;
    textureHeight = 32;
  }
  
  public void setTileSet(PImage tileSet)
  {
    this.tileSet = tileSet;
  }


  
  
  
}
