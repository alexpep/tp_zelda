import java.util.Map;
//*****
//Contient les commandes
//*****

public class RemoteControl
{
  private UpCommand upbutton;
  private DownCommand downbutton;
  private LeftCommand leftbutton;
  private RightCommand rightbutton;
  private QuitCommand quitbutton;
  private HashMap<String, ICommand> hm;
  
  RemoteControl()
  {
    upbutton = new UpCommand();
    downbutton = new DownCommand();
    leftbutton = new LeftCommand();
    rightbutton = new RightCommand();
    quitbutton= new QuitCommand();
    hm  = new HashMap<String, ICommand>();
    
    hm.put("a",leftbutton);
    hm.put("s",downbutton);
    hm.put("d",rightbutton);
    hm.put("w",upbutton);
    hm.put("q",quitbutton);
  }
  
  //*****
  //Appel la bonne commande
  //*****
  void InputHandler()
  {
    String s = Character.toString(key);
    if(hm.containsKey(s))
    {
      hm.get(s).execute();
    }
  }  
  
    void InputHandler(boolean[] w)
  {
    String s = Character.toString(key);
    if(hm.containsKey(s))
    {
      hm.get(s).execute(w);
    }
  }  
}
