class Command{}


class UpCommand implements ICommand {
  
  void execute(){
   
    link.moveUp();
  }
  void execute(boolean[] wall){
     if (!wall[0]) ball.place(ball.x,max(maze.step/2,ball.y-maze.step));
  }
}

class DownCommand implements ICommand {

  void execute(){
   
    link.moveDown();
  }
  
    void execute(boolean[] wall){
    if (!wall[1]) ball.place(ball.x,min(height-maze.step/2,ball.y+maze.step));
  }
}

class LeftCommand implements ICommand {

 
  void execute(){
   
    link.moveLeft();
  }
  
    void execute(boolean[] wall){
    if (!wall[2]) ball.place(max(maze.step/2,ball.x-maze.step),ball.y);
  }
}

class RightCommand implements ICommand {

  void execute(){
    link.moveRight();
  }
  
    void execute(boolean[] wall){
    if (!wall[3]) ball.place(min(width-maze.step/2,ball.x+maze.step),ball.y);
  }
}
  
  
class QuitCommand implements ICommand{

  void execute(){
    
         
     if(maze.complete){   
      maze.state.nextState(maze); 
      mazeIndex = (mazeIndex+1) % mazeSizes.length;    // augmente le maze chaque win
      maze.reset(mazeSizes[mazeIndex]);
      ball.resize(3*maze.step/4);    
      ball.place(maze.x+ maze.step/2, maze.y+maze.step/2);
    }
  }
  
  void execute(boolean[] wall){
    
  }
  
} 
