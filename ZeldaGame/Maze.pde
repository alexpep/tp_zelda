//Observeur
class Maze implements Observateur{
 
  int x;
  int y;
  int w;
  int cellsAcross;
  ArrayList cells;
  boolean show;
 
  int destinationX;
  int destinationY;
 
  int totalCells;
  int visitedCells;
  int currentCell;
  int[] cellStack = new int[1];
  int step;
 
  boolean complete;
  int stateNum;
  State state ;
 
  Maze(int _x, int _y, int _w, int _ca) {
    x=_x;
    y=_y;
    w=_w;
    cellsAcross=_ca;
    show=false;
    state = new State();
 
    step = w/cellsAcross;
    totalCells = cellsAcross*cellsAcross;
    visitedCells = 1;
    currentCell = totalCells-1;
    cellStack[0] = currentCell;
 
    cells = new ArrayList();
    for (int i = 0; i < cellsAcross; i++) {
      for (int j=0; j < cellsAcross; j++) {
        MazeCell c = new MazeCell(x+j*step,y+i*step,step);
        cells.add(c);
      }
    }
    complete = false;
 
    MazeCell lastCell=(MazeCell) cells.get(cells.size()-1);
    lastCell.marked=true;
 
    destinationX=x+w-step;
    destinationY=y+w-step;
  }
 
  void reset(int _ca) {
 
    cellsAcross=_ca;
    step = w/cellsAcross;
 
    for (int i = cells.size()-1; i >=0; i--) {
      cells.remove(i);
    }
 
    for (int i = 0; i < cellsAcross; i++) {
      for (int j=0; j < cellsAcross; j++) {
        MazeCell c = new MazeCell(x+j*step,y+i*step,step);
        cells.add(c);
      }
    }
 
    MazeCell lastCell=(MazeCell) cells.get(cells.size()-1);
    lastCell.marked=true;
 
    while (cellStack.length>1) {
      cellStack = shorten(cellStack);
    }
 
    totalCells = cellsAcross*cellsAcross;
    visitedCells=1;
    currentCell = totalCells-1;
    cellStack[0]=currentCell;
    destinationX=x+w-step;
    destinationY=y+w-step;
    finished = false;
    complete = false;
    randomSeed(millis());
  }
  
    void display() {
    for (int i = 0; i < cells.size(); i++) {
      MazeCell c = (MazeCell) cells.get(i);
      c.display(step/8);
    }
  }
  boolean[] travelThrough(int _x, int _y) {
 
    int index = cellsAcross*((_y-y)/step) + ((_x-x)/step);
    MazeCell inCell = (MazeCell) cells.get(index);
    inCell.visited = min(255,inCell.visited+65);
    return inCell.walls;
  }
 
  void routeStep() {
 

    int numberOfPossibles=0;
 
    int[] neighbors = {
      currentCell-cellsAcross,currentCell+cellsAcross,currentCell-1,currentCell+1
    };
 

    if (currentCell-cellsAcross<0) neighbors[0]=-1;
    if (currentCell+cellsAcross>=cellsAcross*cellsAcross) neighbors[1]=-1;
    if (currentCell%cellsAcross==0) neighbors[2]=-1;
    if (currentCell%cellsAcross==cellsAcross-1) neighbors[3]=-1;
 

    for (int i = 0; i < 4; i++) {
      if (neighbors[i]!=-1) {
        MazeCell c = (MazeCell) cells.get(neighbors[i]);
        if (c.marked) neighbors[i]=-1;
        else numberOfPossibles++;
      }
    }
 
    if (numberOfPossibles>0) { 
      int chosenCell = int(random(numberOfPossibles));
      for (int i = 0; i < 4; i++) {
        if (neighbors[i]!=-1) {
          if (chosenCell==0) {
          
            MazeCell thisCell = (MazeCell) cells.get(currentCell);
            MazeCell nextCell = (MazeCell) cells.get(neighbors[i]);
       
            nextCell.marked=true;
         
            if (i==0) {
              thisCell.walls[0]=false;
              nextCell.walls[1]=false;
            }
            if (i==1) {
              thisCell.walls[1]=false;
              nextCell.walls[0]=false;
            }
            if (i==2) {
              thisCell.walls[2]=false;
              nextCell.walls[3]=false;
            }
            if (i==3) {
              thisCell.walls[3]=false;
              nextCell.walls[2]=false;
            }
            visitedCells++;
            if (visitedCells==totalCells) {
              complete=true;
             
            }
            currentCell = neighbors[i];
            cellStack = append(cellStack,currentCell);
            stroke(0,0,255,5+250*thisCell.x/width);
            strokeWeight(step/2);
            int w = width/(cellsAcross*2);
            line(thisCell.x+step/2,thisCell.y+step/2,nextCell.x+step/2,nextCell.y+step/2);
            break;
          }
          else {
            chosenCell--;
          }
        }
      }
    }
    else {
      currentCell = cellStack[cellStack.length-1];
      cellStack = shorten(cellStack);
    }
  }
  
void update (Observable observable){
  
  state.nextState(maze);
}
  
void setShow(boolean s)
{
  this.show = s;
}

void setEtatMaze(int _state){
  this.stateNum = _state;

}
  
}
