class Rectangle extends GraphicObject{

  private float rectWidth , rectHeight ;
  float left ,right , top , bottom ;
  
  Rectangle(){
    location = new PVector();
    setrectWH(100,100);
  }
  
  Rectangle(float x, float y, float w, float h){
    location = new PVector(x,y);
    setrectWH(w,h);
  }
  
  float getrectWidth(){
    return rectWidth;
  }
  
  float getrectHeight(){
      return rectHeight;
  }
  
  void setrectWH(float rectWidth , float rectHeight){
      this.rectWidth = rectWidth;
      this.rectHeight = rectHeight;
      updateRectCoord();
  }
  
  void setrectXY(float x, float y){
    this.location.x = x;
    this.location.y = y;
    updateRectCoord();
  }
  
  
  
  private void updateRectCoord()
  {
    left = location.x;
    right = left + rectWidth;
    top = location.y;
    bottom = top + rectHeight;
  }

  void update(float delta){
    updateRectCoord();
  }

  void display(){
 
  }


}
