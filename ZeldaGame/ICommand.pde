public interface ICommand{
  public void execute();
  public void execute(boolean[] w);
}
