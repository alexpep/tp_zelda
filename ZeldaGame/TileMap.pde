class TileMap{

  ArrayList<MapRow> rows ;
  int mapWidth , mapHeight ;
  int randJ,randI;
  int min =10;
  TileMap(){
    rows = new ArrayList<MapRow>();
    mapWidth = 75;
    mapHeight =75; 
    randJ=(int) random(1,10);
    randI=(int) random(1,10);
    print(randJ);
    print("iiiii "+ randI);
    
    for (int i = 0; i < mapHeight; i++)
    {
     MapRow rowTemp = new MapRow();
      for (int j = 0; j < mapWidth; j++)
      {
        rowTemp.columns.add(new MapCell(0));
      }
      rows.add(rowTemp);
    }
    
    generateTest();
  }
   
  public MapRow getRow(int rowIndex) {
    return rows.get(rowIndex);
  }

  public int getMapWidth() {
    return mapWidth;
  }

  public void setMapWidth(int mapWidth) {
    this.mapWidth = mapWidth;
  }

  public int getMapHeight() {
    return mapHeight;
  }

  public void setMapHeight(int mapHeight) {
    this.mapHeight = mapHeight;
  }

  private void generateTest() {
    // Début de la création, prob: set des tuiles bizarres 

  int flag= 0 ;
    for(int i =0;i<mapWidth;i++){
      
    
      for(int j=0;j<mapHeight;j++){
        if( j ==randJ && i == randI )
        {
           rows.get(i).columns.get(j).setTileID(7);
           flag = 1;
        }
        else
        {
          rows.get(i).columns.get(j).setTileID(0);
          
        }
      }
    
    
    }
  }

  public void randomCord(int y){
  
    if(y<10)
    {
      randJ=(int) random(min,20);
      randI=(int) random(1,10);
    }
    else{
      randJ=(int) random(1,min);
      randI=(int) random(1,10);
    }
    
    print(randJ);
    print("iiiii "+ randI);
    
   generateTest();
  }
 

}
